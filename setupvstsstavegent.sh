#!/bin/bash

#update and pre-reqs
sudo apt-get update
sudo apt-get install -y openjdk-8-jdk libunwind8 libcurl3

# install docker
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo ubuntu-xenial main" | sudo tee /etc/apt/sources.list.d/docker.list
sudo apt-get update
sudo apt-get install -y docker-engine

# enable docker
sudo systemctl enable docker
sudo systemctl start docker

sudo usermod -a -G docker $1

#make a directory for the agent
mkdir /vstsagent
chown $1:$1 /vstsagent
cd /vstsagent

#get the file
wget https://github.com/Microsoft/vsts-agent/releases/download/v2.107.0/vsts-agent-ubuntu.16.04-x64-2.107.0.tar.gz

#unzip and run
sudo -u $1 tar xvf vsts-agent-ubuntu.16.04-x64-2.107.0.tar.gz
sudo -u $1 bash ./config.sh --unattended --url $2 --auth PAT --token $3 --pool default --agent $4

#install as a service
sudo ./svc.sh install
sudo ./svc.sh start